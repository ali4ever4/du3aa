package net.omandev.du3aa;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.getSettings().setLoadsImagesAutomatically(true);
        myWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        AssetManager assetManager = getAssets();
        try {
            InputStream ims = assetManager.open("index.html");
            StringBuilder inputStringBuilder = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ims, "UTF-8"));
            String line = bufferedReader.readLine();
            while (line != null) {
                inputStringBuilder.append(line);
                inputStringBuilder.append('\n');
                line = bufferedReader.readLine();
            }

            String index = inputStringBuilder.toString();
            //  myWebView.loadDataWithBaseURL("file:///android_asset/", index, "text/html", "UTF-8", null);
        } catch (Exception e) {

        }
        myWebView.loadUrl("file:///android_asset/index.html");


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
